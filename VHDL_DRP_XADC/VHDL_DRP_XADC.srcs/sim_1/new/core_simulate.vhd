----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.02.2016 16:59:00
-- Design Name: 
-- Module Name: core_simulate - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity core_simulate is
--  Port ( );
end core_simulate;

architecture Behavioral of core_simulate is
component core is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           D : in STD_LOGIC;
           Q : out STD_LOGIC);
end component core;

signal clk, rst, D, Q: STD_LOGIC;
constant clk_period:time:= 100 ns;
begin

UUI: core Port map (clk => clk, rst => rst, Q => Q, D => D);
clk_process:process begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
end process;
test:process begin 
    clk <= '0';
    wait for 120ms;
    D <= '0';
    wait for 120ms;
    D <= '1';
    wait for 120ms;
    D <= '0';
    wait for 120ms;
    D <= '1';
    wait for 30ms;
    rst <= '1';
    wait for 120ms;
    D <= '0';
    wait for 30ms;
    rst <= '0';
    wait for 120ms;
    D <= '1';
end process;

end Behavioral;
