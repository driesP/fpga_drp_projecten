----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.02.2016 16:34:46
-- Design Name: 
-- Module Name: core - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity core is
    Port ( clk : in STD_LOGIC;
           led : out STD_LOGIC_VECTOR (12 downto 8);
           );
end core;

architecture Behavioral of core is

component xadc_wiz_0 is
   port
   (
    dclk_in         : in  STD_LOGIC;                         -- Clock input for the dynamic reconfiguration port
    busy_out        : out  STD_LOGIC;                        -- ADC Busy signal
    channel_out     : out  STD_LOGIC_VECTOR (4 downto 0);    -- Channel Selection Outputs
    eoc_out         : out  STD_LOGIC;                        -- End of Conversion Signal
    eos_out         : out  STD_LOGIC;                        -- End of Sequence Signal
    alarm_out       : out STD_LOGIC;                         -- OR'ed output of all the Alarms
    muxaddr_out     : out STD_LOGIC_VECTOR(4 downto 0); 
    vp_in           : in  STD_LOGIC;                         -- Dedicated Analog Input Pair
    vn_in           : in  STD_LOGIC
);
end component;
begin
xadc: xadc_wiz_0
    port map(
    dclk_in => clk,
    busy_out => led (13 downto 13),
    channel_out => led (12 downto 8),
    eoc_out => led (7 downto 7),
    eos_out => led (6 downto 6),
    alarm_out => led (5 downto 5),
    muxaddr_out => led (4 downto 0),
    vp_in => '1',
    vn_in => '1'
    );
    
 process (clk) begin
 
 end process;

end Behavioral;
