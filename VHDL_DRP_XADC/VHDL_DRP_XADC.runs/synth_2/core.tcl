# 
# Synthesis run script generated by Vivado
# 

set_param xicom.use_bs_reader 1
set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000
create_project -in_memory -part xc7a35tcpg236-1

set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_property webtalk.parent_dir {C:/Users/Dries Peeters/Documents/Sources/VHDL_DRP_XADC/VHDL_DRP_XADC.cache/wt} [current_project]
set_property parent.project_path {C:/Users/Dries Peeters/Documents/Sources/VHDL_DRP_XADC/VHDL_DRP_XADC.xpr} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language VHDL [current_project]
set_property board_part digilentinc.com:basys3:part0:1.1 [current_project]
set_property vhdl_version vhdl_2k [current_fileset]
add_files -quiet {{C:/Users/Dries Peeters/Documents/Sources/VHDL_DRP_XADC/VHDL_DRP_XADC.runs/xadc_wiz_0_synth_1/xadc_wiz_0.dcp}}
set_property used_in_implementation false [get_files {{C:/Users/Dries Peeters/Documents/Sources/VHDL_DRP_XADC/VHDL_DRP_XADC.runs/xadc_wiz_0_synth_1/xadc_wiz_0.dcp}}]
read_vhdl -library xil_defaultlib {{C:/Users/Dries Peeters/Documents/Sources/VHDL_DRP_XADC/VHDL_DRP_XADC.srcs/sources_1/new/core.vhd}}
read_xdc {{C:/Users/Dries Peeters/Documents/Sources/VHDL_DRP_XADC/VHDL_DRP_XADC.srcs/constrs_1/imports/Sources/Basys3_Master.xdc}}
set_property used_in_implementation false [get_files {{C:/Users/Dries Peeters/Documents/Sources/VHDL_DRP_XADC/VHDL_DRP_XADC.srcs/constrs_1/imports/Sources/Basys3_Master.xdc}}]

synth_design -top core -part xc7a35tcpg236-1
write_checkpoint -noxdef core.dcp
catch { report_utilization -file core_utilization_synth.rpt -pb core_utilization_synth.pb }
