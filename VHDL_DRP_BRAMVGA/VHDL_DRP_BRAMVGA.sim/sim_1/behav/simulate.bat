@echo off
set xv_path=D:\\xilinx\\Vivado\\2015.4\\bin
call %xv_path%/xsim vgaSim_behav -key {Behavioral:sim_1:Functional:vgaSim} -tclbatch vgaSim.tcl -log simulate.log
if "%errorlevel%"=="0" goto SUCCESS
if "%errorlevel%"=="1" goto END
:END
exit 1
:SUCCESS
exit 0
