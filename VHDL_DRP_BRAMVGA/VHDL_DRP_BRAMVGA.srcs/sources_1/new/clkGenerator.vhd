----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.03.2016 16:43:39
-- Design Name: 
-- Module Name: clkGenerator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clkGenerator is
    Port ( rawClk : in STD_LOGIC;
           outClk : out STD_LOGIC);
end clkGenerator;

architecture Behavioral of clkGenerator is

signal clk50: std_logic:='0';

begin
process (rawClk) begin
if rising_edge(rawClk) then
    clk50 <= not clk50;
  end if;
end process;


outClk <= clk50;

end Behavioral;
