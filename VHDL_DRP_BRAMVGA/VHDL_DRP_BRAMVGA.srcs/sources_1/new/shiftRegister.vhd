----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.05.2016 10:26:12
-- Design Name: 
-- Module Name: shiftRegister - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shiftRegister is
    Port ( clkLow : in STD_LOGIC;
           input : in STD_LOGIC_VECTOR (7 downto 0);
           output : out STD_LOGIC_VECTOR(7 DOWNTO 0);
           sample : in STD_LOGIC;
           address : in integer
           );
end shiftRegister;

architecture Behavioral of shiftRegister is
type sr640x8 is array (0 to 639) of STD_LOGIC_VECTOR(7 DOWNTO 0);
signal regData : sr640x8;
signal count : integer := 0;
begin

process (clkLow) begin
    if (rising_edge(clkLow)) then
        if(sample = '1') then
            regData(1 to 639) <= regData(0 to 638);
            regData(0)<= input;
        end if;
    end if;
end process;
process (clkLow) begin
    if(rising_edge(clkLow)) then
        output <= regData(address);
    end if;
end process;
end Behavioral;

