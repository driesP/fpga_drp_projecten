----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.03.2016 15:43:39
-- Design Name: 
-- Module Name: sync_driver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sync_driver is
    Port ( clk : in STD_LOGIC;
           hSync : out STD_LOGIC;
           vSync : out STD_LOGIC;
           x : out STD_LOGIC_VECTOR(9 downto 0);
           y : out STD_LOGIC_VECTOR(9 downto 0)
           );
end sync_driver;

architecture Behavioral of sync_driver is

signal horizontalCount: std_logic_vector (9 downto 0);
signal verticalCount: std_logic_vector (9 downto 0);
signal clk25: STD_LOGIC:= '0';

begin

process(clk) begin
if(rising_edge(clk)) then
    clk25 <= Not clk25;
end if;
end process;



process (clk25) begin
if (rising_edge(clk25)) then
    horizontalCount <= horizontalCount + 1;
    if(horizontalCount = "0001100000") then
        hSync <= '1';
    end if;
    if(horizontalCount = "1100100000") then
        hSync <= '0';
        horizontalCount <= "0000000000";
        verticalCount <= verticalCount +1;
    end if;
    if(verticalCount = "0000000010") then
        vSync <= '1';
    end if;
    if(verticalCount = "1000001001") then
        vSync <= '0';
        verticalCount <= "0000000000";
    end if;
    
end if;
end process;
x <= horizontalCount;
y <= verticalCount;
end Behavioral;
