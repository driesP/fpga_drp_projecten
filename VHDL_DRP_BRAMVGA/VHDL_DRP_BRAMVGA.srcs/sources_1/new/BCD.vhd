----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.05.2016 16:19:45
-- Design Name: 
-- Module Name: BCD - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BCD is
    Port ( clk : in STD_LOGIC;
           dataIn : in STD_LOGIC_VECTOR (7 downto 0);
           digit1 : out integer;
           digit2 : out integer;
           digit3 : out integer);
end BCD;

architecture Behavioral of BCD is
signal i: integer := 0;
signal bcd: STD_LOGIC_VECTOR(11 DOWNTO 0);
signal bint : STD_LOGIC_VECTOR (7 DOWNTO 0) := dataIn;
signal value  : integer := 0;
signal temp1 : integer := 0;
signal temp2 : integer := 0;
signal d1 : integer := 0;
signal d2 : integer := 0;
signal d3 : integer := 0;

begin

process (clk) begin
if(rising_edge(clk))then
   
    value <= ((1000/255)+1)* conv_integer(dataIn);
    d1 <= value/100;
    temp1 <= value - (d1*100);
    d2 <= temp1/10;
    temp2 <= temp1 -(d2*10);
    d3 <= temp2;
   
    
digit1 <=  d1; 
    
digit2 <=  d2;
digit3 <=  d3;

end if;
end process;

end Behavioral;
