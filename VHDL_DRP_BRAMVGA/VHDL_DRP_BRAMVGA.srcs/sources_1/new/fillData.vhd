----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.05.2016 11:09:50
-- Design Name: 
-- Module Name: fillData - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fillData is
    Port ( weaI : in STD_LOGIC;
           weaO : out STD_LOGIC;
           clk : in STD_LOGIC;
           data : in STD_LOGIC_VECTOR (7 DOWNTO 0)
           );
end fillData;

architecture Behavioral of fillData is

component shiftRegister is
    Port ( clk25 : in STD_LOGIC;
           input : in STD_LOGIC_VECTOR (7 downto 0);
           output : out STD_LOGIC_VECTOR (7 downto 0);
           address : in STD_LOGIC_VECTOR (9 DOWNTO 0);
           wea: in STD_LOGIC
           );
end component;

signal clk25 : std_logic := '0';
signal a : std_logic := '0';
signal wea : std_logic := '0';

begin
shift: shiftRegister
    port map(
    clk25 => clk25,
    input => data,
    output => open,
    address => "0000000000",
    wea => wea
    );
    
    
process (clk) begin 
wea <= weaI;
if(rising_edge(clk)) then 
if(a = '0') then
    weaO <= '1';
    wea <= '1';
    a <= '1';
else
    weaO <= '0';
    wea <= '0';
    a <= '0';
end if;   
end if;
end process;



end Behavioral;
