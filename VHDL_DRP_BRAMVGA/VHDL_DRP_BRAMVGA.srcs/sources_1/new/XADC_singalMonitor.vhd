----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.05.2016 13:50:07
-- Design Name: 
-- Module Name: XADC_singalMonitor - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity XADC_singalMonitor is
    Port ( m_data_in : in STD_LOGIC_VECTOR (7 downto 0);
           m_clk_in : in STD_LOGIC;
           m_reset : in STD_lOGIC;
           m_data_out : out STD_LOGIC_VECTOR (7 downto 0);
           m_sample : out STD_LOGIC;
           m_led : out STD_LOGIC_VECTOR (7 DOWNTO 0));
end XADC_singalMonitor;

architecture Behavioral of XADC_singalMonitor is

type state_type is (st_start, st_stop, st_idle);
signal state : state_type := st_idle;
signal first_init : STD_LOGIC := '1';
signal store_signal : STD_LOGIC_VECTOR (7 DOWNTO 0);
signal counter : integer := 0;
signal i_led : STD_LOGIC:= '0';
signal i_reset : STD_LOGIC := '0';
signal bounce : STD_LOGIC := '0';

begin

process(m_clk_in,m_reset) begin
    if(rising_edge(m_reset)) then
        if( bounce = '0') then
            i_reset <= '1';
            bounce <= '1';
        else
            bounce <= '0';
            i_reset <= '0';
        end if;
    end if;
    if(rising_edge(m_clk_in)) then
        
        case state is
            when st_idle =>
                if(i_reset = '1') then
                    state <= st_start;
                end if;
            when st_start =>
                m_sample <= '1';
                m_data_out <= m_data_in;
                if(counter = 639) then
                    counter <= 0;
                    state <= st_stop;
                else
                    counter <= counter +1;
                end if;              
            when st_stop =>
                m_sample <= '0';               
                store_signal <= "00000000";
                m_data_out <= "00000000";
                state <= st_idle;
        end case;
    end if;
end process;
end Behavioral;
