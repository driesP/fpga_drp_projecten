----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.03.2016 17:13:59
-- Design Name: 
-- Module Name: main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
    Port (
           vgaRed : out STD_LOGIC_VECTOR (3 downto 0);
           vgaGreen : out STD_LOGIC_VECTOR (3 downto 0);
           vgaBlue : out STD_LOGIC_VECTOR (3 downto 0);
           Hsync : out STD_LOGIC;
           Vsync : out STD_LOGIC;
           clk : in STD_LOGIC;
           sw : in STD_LOGIC_VECTOR(3 downto 0);
           led : out STD_LOGIC_VECTOR(15 DOWNTO 0);
           JXADC : in STD_LOGIC_VECTOR(7 DOWNTO 0);
           btnC : in STD_LOGIC
           );
end main;

architecture Behavioral of main is

signal reading : std_logic_vector(15 downto 0) := (others => '0');
signal muxaddr : std_logic_vector( 4 downto 0) := (others => '0');
signal channel : std_logic_vector( 4 downto 0) := (others => '0');
signal vauxn   : std_logic_vector(15 downto 0) := (others => '0');
signal vauxp   : std_logic_vector(15 downto 0) := (others => '0');
signal clk25 : std_logic := '0';
signal x: STD_LOGIC_VECTOR (9 downto 0);
signal y: STD_LOGIC_VECTOR (9 downto 0);
signal clk50: STD_LOGIC:= '0';
signal digit1 : integer := 0;
signal digit2 : integer := 0;
signal digit3 : integer := 0;
signal dataOut: STD_LOGIC_VECTOR (7 DOWNTO 0);
signal sample : STD_LOGIC;

component clkGenerator is
    Port ( rawClk : in STD_LOGIC;
           outClk : out STD_LOGIC);
end component;

component VGA_driver is
    Port ( vgaRed : out STD_LOGIC_VECTOR (3 downto 0);
           vgaGreen : out STD_LOGIC_VECTOR (3 downto 0);
           vgaBlue : out STD_LOGIC_VECTOR (3 downto 0);
           clk : in STD_LOGIC;
           clk2 : in STD_LOGIC;
           x: in STD_LOGIC_VECTOR (9 downto 0);
           y: in STD_LOGIC_VECTOR (9 downto 0);
           data : in STD_LOGIC_VECTOR (7 DOWNTO 0);
           sample : in STD_LOGIC;
           digit1: in integer;
           digit2: in integer;
           digit3: in integer
           );
end component;

component sync_driver is
    Port ( clk : in STD_LOGIC;
           hSync : out STD_LOGIC;
           vSync : out STD_LOGIC;
           x : out STD_LOGIC_VECTOR(9 downto 0);
           y : out STD_LOGIC_VECTOR(9 downto 0)
           );
end component;

component XADC_PR is
   port
   (
    daddr_in        : in  STD_LOGIC_VECTOR (6 downto 0);     -- Address bus for the dynamic reconfiguration port
    den_in          : in  STD_LOGIC;                         -- Enable Signal for the dynamic reconfiguration port
    di_in           : in  STD_LOGIC_VECTOR (15 downto 0);    -- Input data bus for the dynamic reconfiguration port
    dwe_in          : in  STD_LOGIC;                         -- Write Enable for the dynamic reconfiguration port
    do_out          : out  STD_LOGIC_VECTOR (15 downto 0);   -- Output data bus for dynamic reconfiguration port
    drdy_out        : out  STD_LOGIC;                        -- Data ready signal for the dynamic reconfiguration port
    dclk_in         : in  STD_LOGIC;                         -- Clock input for the dynamic reconfiguration port
    reset_in        : in  STD_LOGIC;                         -- Reset signal for the System Monitor control logic
    vauxp6          : in  STD_LOGIC;                         -- Auxiliary Channel 6
    vauxn6          : in  STD_LOGIC;
    busy_out        : out  STD_LOGIC;                        -- ADC Busy signal
    channel_out     : out  STD_LOGIC_VECTOR (4 downto 0);    -- Channel Selection Outputs
    eoc_out         : out  STD_LOGIC;                        -- End of Conversion Signal
    eos_out         : out  STD_LOGIC;                        -- End of Sequence Signal
    alarm_out       : out STD_LOGIC;                         -- OR'ed output of all the Alarms
    vp_in           : in  STD_LOGIC;                         -- Dedicated Analog Input Pair
    vn_in           : in  STD_LOGIC
);
end component;


component BCD is
    Port ( clk : in STD_LOGIC;
           dataIn : in STD_LOGIC_VECTOR (7 downto 0);
           digit1 : out integer;
           digit2 : out integer;
           digit3 : out integer);
end component;

component XADC_singalMonitor is
    Port ( m_data_in : in STD_LOGIC_VECTOR (7 downto 0);
           m_clk_in : in STD_LOGIC;
           m_reset : in STD_lOGIC;
           m_data_out : out STD_LOGIC_VECTOR (7 downto 0);
           m_sample : out STD_LOGIC;
           m_led : out STD_LOGIC_VECTOR (7 DOWNTO 0));
end component;

begin

led(15 downto 8) <= dataOut;

clkGen: clkGenerator
    Port map(
    rawClk => clk,
    outClk => clk50
    );
VGA: VGA_driver
    Port map(
    vgaRed => vgaRed,
    vgaGreen => vgaGreen,
    vgaBlue => vgaBlue,
    clk => clk50,
    clk2 => clk,
    x => x,
    y => y,
    data => dataOut,
    digit1 => digit1,
    digit2 => digit2,
    digit3 => digit3,
    sample => sample
    );
Sync: sync_driver
    port map(
    clk => clk50,
    hSync => Hsync,
    vSync => Vsync,
    x => x,
    y => y
    );
    
xadc: XADC_PR
    port map(
       daddr_in => "0010110",     -- Address bus for the dynamic reconfiguration port
       den_in => '1',                         -- Enable Signal for the dynamic reconfiguration port
       di_in => (others => '0'),   -- Input data bus for the dynamic reconfiguration port
       dwe_in => '0',                        -- Write Enable for the dynamic reconfiguration port
       do_out  => reading, -- Output data bus for dynamic reconfiguration port
       drdy_out => open,                       -- Data ready signal for the dynamic reconfiguration port
       dclk_in => clk,                        -- Clock input for the dynamic reconfiguration port
       reset_in  => '0',                       -- Reset signal for the System Monitor control logic
       vauxp6 => JXADC(0),                      -- Auxiliary Channel 6
       vauxn6 => JXADC(4),
       busy_out  => open,                      -- ADC Busy signal
       channel_out => channel,   -- Channel Selection Outputs
       eoc_out => open,                      -- End of Conversion Signal
       eos_out => open,                       -- End of Sequence Signal
       alarm_out => open,                      -- OR'ed output of all the Alarms
       vp_in => '0',                        -- Dedicated Analog Input Pair
       vn_in => '0'
    );
b: bcd
    port map(
    clk => clk25,
    dataIn => reading(15 downto 8),
    digit1 => digit1,
    digit2 => digit2,
    digit3 => digit3
    );
monitor: XADC_singalMonitor
    port map(
    m_data_in => reading (15 downto 8),
    m_clk_in => clk25,
    m_data_out => dataOut,
    m_sample => sample,
    m_led => led(7 downto 0),
    m_reset => btnC
    );
process(clk) begin
if(rising_edge(clk)) then
    clk25 <= Not clk25;
end if;
end process;
end Behavioral;