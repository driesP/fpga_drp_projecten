----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.03.2016 16:40:01
-- Design Name: 
-- Module Name: VGA_driver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VGA_driver is
    Port ( vgaRed : out STD_LOGIC_VECTOR (3 downto 0);
           vgaGreen : out STD_LOGIC_VECTOR (3 downto 0);
           vgaBlue : out STD_LOGIC_VECTOR (3 downto 0);
           clk : in STD_LOGIC;
           clk2 : in STD_LOGIC;
           x: in STD_LOGIC_VECTOR (9 downto 0);
           y: in STD_LOGIC_VECTOR (9 downto 0);
           data : in STD_LOGIC_VECTOR (7 DOWNTO 0);
           sample : in STD_LOGIC;
           digit1: in integer;
           digit2: in integer;
           digit3: in integer
           );
end VGA_driver;

architecture Behavioral of VGA_driver is

signal clk25 : STD_LOGIC:= '0';
signal clk_0_5 : STD_LOGIC:= '0';
Signal startX : STD_LOGIC_VECTOR (9 DOWNTO 0):= "0010010000";
Signal startY : STD_LOGIC_VECTOR (9 DOWNTO 0):= "0000100111";
signal da : integer := 0;
signal do : STD_LOGIC_VECTOR (7 DOWNTO 0) := "00000000";
signal i : integer := 0;
signal count : integer := 0;
signal countA : integer := 0;
signal digitB0 : integer := 0;
signal digitB1 : integer := 16;
signal digitB2 : integer := 176;
signal digitB3 : integer := 16;
signal digitB4 : integer := 16;
signal digitB5 : integer := 16;
signal digitB6 : integer := 528;
signal sa :integer := 0;
signal so : STD_LOGIC_VECTOR (7 DOWNTO 0) := "00000000";
signal value : STD_LOGIC_VECTOR(7 downto 0) := "11111111";
signal wea : STD_LOGIC := '0';
signal counter : integer := 0;
signal countClk : integer := 0;
signal clk39Khz : std_logic := '0';

component Font is
    Port ( address : in integer;
           data : out STD_LOGIC_VECTOR (7 downto 0);
           clk : in STD_LOGIC);
end component;

component shiftRegister is
    Port ( clkLow : in STD_LOGIC;
           input : in STD_LOGIC_VECTOR (7 downto 0);
           output : out STD_LOGIC_VECTOR(7 DOWNTO 0);
           sample : in STD_LOGIC;
           address : in integer
           );
end component;

begin

digit_1: Font
    port map
    (
    clk => clk2,
    address => da,
    data => do
    );
signaal: shiftRegister
    port map
    (
        clkLow => clk25,
        address => sa,
        sample => sample,
        input => data,
        output => so
    );
process(clk) begin
if(rising_edge(clk)) then
    clk25 <= Not clk25;
end if;
end process;
process(clk25) begin
if(rising_edge(clk25)) then
    if(counter = 100000) then
        clk_0_5 <= not clk_0_5;
        counter <= 0;
        digitB3 <= (digit1 + 1) *16;
        digitB4 <= (digit2 + 1) *16;
        digitB5 <= (digit3 + 1) *16;
    else
        counter <= counter + 1;
    end if;
end if;
end process;


process (clk25) begin   
if rising_edge(clk25) 
then
    if (x >= startX ) -- 144
    and (x < startX + 640 ) -- 784
    and (y >= startY ) -- 39
    and (y < startY + 480 ) -- 519
    then
   
        
        
       --DRAW THE TWO LINES
    
        if( (y = startY + 124) OR (y = startY + 125) OR (y = startY + 380) OR (y = startY + 381))
        then           
            vgaRed <= "1111";
            vgaGreen <= "1111";
            vgaBlue <= "1111";
        end if;
                      
        if((y >= startY + 50) AND (y < startY + 66) AND  (x >= startX + 50) AND  (x < startX + 122))
        then          
            case(i) is
            when 0 => da <= digitB0 + countA;
            when 1 => da <= digitB1 + countA;
            when 2 => da <= digitB2 + countA;
            when 3 => da <= digitB3 + countA;
            when 4 => da <= digitB4 + countA;
            when 5 => da <= digitB5 + countA;
            when 6 => da <= digitB6 + countA;
            when 7 => da <= digitB0 + countA;
            when others => da <= digitB1 + countA;
            end case;
            if(count = 8) then
                vgaRed <= "0000";
                vgaGreen <= "0000";
                vgaBlue <= "0000";
            else
            if(do(count) = '1')then 
                vgaRed <= "1111";
                vgaGreen <= "1111";
                vgaBlue <= "1111"; 
            else 
                vgaRed <= "0000";
                vgaGreen <= "0000";
                vgaBlue <= "0000"; 
            end if;                                  
            end if;
            if(count = 0)
            then 
                count <= 8;
                if (i = 7) then
                    if(countA = 15)
                    then 
                        countA <= 0;
                    else
                        countA <= countA + 1;
                    end if;
                    i <= 0;
                else
                    i <= i + 1;
                end if;
            else
                count  <= count - 1;             
            end if;                                                 
        end if; 
        -->DRAW THE LINE
        
        if((y >= startY + 126) AND (y < startY + 380) AND (x >= (startX)) AND (x < (startX + 640)))          
        then
            if((x = (startX +sa))) then
                if(so = value) then
                    vgaRed <= "1111";
                    vgaGreen <= "1111";
                    vgaBlue <= "1111";
                else
                    vgaRed <= "0000";
                    vgaGreen <= "0000";
                    vgaBlue <= "0000";
                         
                end if;
                if(sa = 639)
                then
                    sa <= 0;
                    value <= value -1;
                else
                    sa <= sa +1;
                end if; 
                if(value = "00000001")
                then
                    value <= "11111111";
                end if;
                      
            end if;                 
        end if;
        
        
    else
        vgaRed <= "0000";
        vgaGreen <= "0000";
        vgaBlue <= "0000";
    end if;
    
end if;
end process;
end Behavioral;
