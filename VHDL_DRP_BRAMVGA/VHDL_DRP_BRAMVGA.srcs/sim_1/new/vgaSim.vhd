----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.05.2016 23:32:05
-- Design Name: 
-- Module Name: vgaSim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vgaSim is
--  Port ( );
end vgaSim;

architecture Behavioral of vgaSim is

component VGA_driver is
    Port ( vgaRed : out STD_LOGIC_VECTOR (3 downto 0);
           vgaGreen : out STD_LOGIC_VECTOR (3 downto 0);
           vgaBlue : out STD_LOGIC_VECTOR (3 downto 0);
           clk : in STD_LOGIC;
           clk2 : in STD_LOGIC;
           x: in STD_LOGIC_VECTOR (9 downto 0);
           y: in STD_LOGIC_VECTOR (9 downto 0);
           data : in STD_LOGIC_VECTOR (7 DOWNTO 0);
           sample : in STD_LOGIC;
           digit1: in integer;
           digit2: in integer;
           digit3: in integer
           );
end component;

signal vgaRed : STD_LOGIC_VECTOR(3 downto 0) := "0000";
signal vgaGreen : STD_LOGIC_VECTOR(3 downto 0) :="0000";
signal vgaBlue : STD_LOGIC_VECTOR(3 downto 0) :="0000";
signal clk : STD_LOGIC := '0';
signal clk2 : STD_LOGIC := '0';
signal x : STD_LOGIC_VECTOR(9 DOWNTO 0) := "0000000000";
signal y : STD_LOGIC_VECTOR(9 DOWNTO 0) := "0000000000";
signal data : STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000000";
signal sample : STD_LOGIC := '0';
signal digit1 : integer := 0;
signal digit2 : integer := 0;
signal digit3 : integer := 0;

begin

uut: VGA_driver
    port map(
    vgaRed => vgaRed,
    vgaGreen => vgaBlue,
    vgaBlue => vgaBlue,
    clk => clk,
    clk2 => clk2,
    x => x,
    y => y,
    data => data,
    sample => sample,
    digit1 => digit1,
    digit2 => digit2,
    digit3 => digit3   
    );
process begin
    clk <= not clk;
    wait for 20 ps;
end process;
process begin
    clk2 <= not clk2;
    wait for 5 ps;     
end process;

process(clk) begin
    if(x = "1111111111")then
        x <= "0000000000";
    else
        x <= x + 1;
    end if;
    if(y = "1111111111")then
        y <= "0000000000";
    else
        y <= y +1;
    end if;
    if(data = "11111111")then
        data <= "00000000";
    else
        data <= data +1;
    end if;
    if(digit1 = 9) then
        digit1 <= 0;
    else
        digit1 <= digit1 + 1;
    end if;
    if(digit2 = 9) then
        digit2 <= 0;
    else
        digit2 <= digit2 + 1;
    end if; 
    if(digit3 = 9) then
        digit3 <= 0;
    else
        digit3 <= digit3 + 1;
    end if;   
end process;

process begin
    sample <= not sample;
    wait for 15000 ps;
end process;

end Behavioral;
