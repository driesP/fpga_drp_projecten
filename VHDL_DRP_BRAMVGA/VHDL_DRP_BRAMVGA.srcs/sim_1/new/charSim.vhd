----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.05.2016 20:46:06
-- Design Name: 
-- Module Name: charSim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity charSim is
--  Port ( );
end charSim;

architecture Behavioral of charSim is

component Font is
    Port ( address : in integer;
           data : out STD_LOGIC_VECTOR (7 downto 0);
           clk : in STD_LOGIC);
end component;

signal address : integer := 0;
signal data : STD_LOGIC_VECTOR (7 DOWNTO 0) := "00000000";
signal clk : STD_LOGIC := '0';
begin

uut: Font
    port map(
    address => address,
    data => data,
    clk => clk
    );

process begin
    clk <= '0';
    wait for 10 ps;
    clk <= '1';
    wait for 10 ps;
end process;

process (clk) begin

if(rising_edge(clk)) then 
    if(address = 607) then 
        address <= 0;
    else
        address <= address + 1;
    end if;
end if;

end process;

end Behavioral;
