----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.05.2016 21:04:20
-- Design Name: 
-- Module Name: bcdSim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bcdSim is
--  Port ( );
end bcdSim;



architecture Behavioral of bcdSim is

component BCD is
    Port ( clk : in STD_LOGIC;
           dataIn : in STD_LOGIC_VECTOR (7 downto 0);
           digit1 : out integer;
           digit2 : out integer;
           digit3 : out integer);
end component;
signal clk : STD_LOGIC := '0';
signal dataIn : STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000000";
signal digit1, digit2, digit3 : integer := 0;
begin

uut: BCD
    port map(
    clk => clk,
    dataIn => dataIn,
    digit1 => digit1,
    digit2 => digit2,
    digit3 => digit3
    );

process begin
    clk <= '0';
    wait for 10 ps;
    clk <= '1';
    wait for 10 ps;
end process;

process (clk) begin

if(rising_edge(clk)) then 
    if(dataIn = "11111111") then 
        dataIn <= "00000000";
    else
        dataIn <= dataIn + 1;
    end if;
end if;
end process;

end Behavioral;
