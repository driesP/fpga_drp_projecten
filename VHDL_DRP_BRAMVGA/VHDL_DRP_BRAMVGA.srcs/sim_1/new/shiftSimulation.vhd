----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.05.2016 09:18:39
-- Design Name: 
-- Module Name: shiftSimulation - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shiftSimulation is
--  Port ( );
end shiftSimulation;

architecture Behavioral of shiftSimulation is


component shiftRegister is
    Port ( clkLow : in STD_LOGIC;
           input : in STD_LOGIC_VECTOR (7 downto 0);
           output : out STD_LOGIC_VECTOR(7 DOWNTO 0);
           sample : in STD_LOGIC;
           address : in integer
           );
end component;

signal clkLow : STD_LOGIC := '0';
signal input : STD_LOGIC_VECTOR (7 DOWNTO 0) := "00000000";
signal output : STD_LOGIC_VECTOR (7 DOWNTO 0) := "00000000";
signal a : integer := 0;
signal sample : STD_LOGIC := '0';

begin

uut: shiftRegister
    port map(
    clkLow => clkLow,
    input => input,
    output => output,
    address => a,
    sample => sample
    );

process begin
    clkLow <= '0';
    wait for 10 ps;
    clkLow <= '1';
    wait for 10 ps;
end process;

process( clkLow) begin
if(rising_edge(clkLow)) then
    input <= input + 1;     
    if(input = "11111111") then
        input <= "00000000";
    end if;
    
end if;
if(rising_edge(clkLow)) then
    a <= a + 1;
    if (a <= 649) then
            a <= 0;
    end if;
end if;

end process;
process begin
    sample <= '1';
    wait for 150 ps;
    sample <= '0';
    wait for 150 ps;
end process;
end Behavioral;
