----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.05.2016 20:19:41
-- Design Name: 
-- Module Name: clkSim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clkSim is
--  Port ( );
end clkSim;

architecture Behavioral of clkSim is

component clkGenerator is
    Port ( rawClk : in STD_LOGIC;
           outClk : out STD_LOGIC);
end component;
signal clkIn : STD_LOGIC := '0';
signal clkOut : STD_LOGIC := '0';
begin

uut: clkGenerator
    port map(
    rawClk => clkIn,
    outClk => clkOut
    );

process begin
    clkIn <= '1';
    wait for 20ps;
    clkIn <= '0';
    wait for 20ps;
end process;

end Behavioral;
