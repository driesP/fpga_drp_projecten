----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.05.2016 23:05:36
-- Design Name: 
-- Module Name: syncSim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity syncSim is
--  Port ( );
end syncSim;

architecture Behavioral of syncSim is

component sync_driver is
    Port ( clk : in STD_LOGIC;
           hSync : out STD_LOGIC;
           vSync : out STD_LOGIC;
           x : out STD_LOGIC_VECTOR(9 downto 0);
           y : out STD_LOGIC_VECTOR(9 downto 0)
           );
end component;

signal clk : STD_LOGIC := '0';
signal hSync : STD_LOGIC := '0';
signal vSync : STD_LOGIC := '0';
signal x : STD_LOGIC_VECTOR(9 DOWNTO 0) := "0000000000";
signal y : STD_LOGIC_VECTOR(9 DOWNTO 0) := "0000000000";
signal test : std_logic := '0';

begin
uut: sync_driver 
    port map(
    clk => clk,
    hSync => hSync,
    vSync => vSync,
    x => x,
    y => y
    );
process begin
    clk <= not clk;
    wait for 20 ps;   
end process;

process(clk) begin
     test <= not test;
end process;

end Behavioral;
