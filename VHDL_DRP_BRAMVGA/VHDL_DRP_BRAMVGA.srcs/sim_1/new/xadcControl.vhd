----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.05.2016 21:36:48
-- Design Name: 
-- Module Name: xadcControl - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity xadcControl is
--  Port ( );
end xadcControl;

architecture Behavioral of xadcControl is

component XADC_singalMonitor is
    Port ( m_data_in : in STD_LOGIC_VECTOR (7 downto 0);
           m_clk_in : in STD_LOGIC;
           m_reset : in STD_lOGIC;
           m_data_out : out STD_LOGIC_VECTOR (7 downto 0);
           m_sample : out STD_LOGIC;
           m_led : out STD_LOGIC_VECTOR (7 DOWNTO 0));
end component;

signal clk : STD_LOGIC := '0';
signal data : STD_LOGIC_VECTOR (9 DOWNTO 0) := "0000000000";
signal reset : STD_LOGIC := '0';
signal data_out : STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000000";
signal sample : STD_LOGIC := '0';
signal reset_sig : STD_LOGIC := '0';

begin

uut: XADC_singalMonitor
    port map
    (
        m_data_in => data(7 DOWNTO 0),
        m_clk_in => clk,
        m_reset => reset,
        m_data_out => data_out,
        m_sample => sample,
        m_led => open
    );
process begin
    clk <= '1';
    wait for 20 ps;
    clk <= '0';
    wait for 20ps;
end process;

process (clk) begin
    if(rising_edge(clk))then
        for I in 0 to 1023 loop
            data <= data + 1;
        end loop;
        
    end if;
end process;
process begin
if(reset_sig = '0') then
    wait for 200 ps;
    reset <= '1';
    wait for 200 ps;
    reset <= '0';
    reset_sig <='1';
else
    wait for 15000 ps;
    reset_sig <= '0';
end if;
end process; 
end Behavioral;
