----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.02.2016 15:21:36
-- Design Name: 
-- Module Name: vhdlDriver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vhdlDriver is
    Port ( clk : in STD_LOGIC;
           Vsync: out STD_LOGIC;
           Hsync: out STD_LOGIC;
           vgaRed: out STD_LOGIC_VECTOR(3 downto 0);
           vgaGreen: out STD_LOGIC_VECTOR(3 downto 0);
           vgaBlue: out STD_LOGIC_VECTOR(3 downto 0)
           );
end vhdlDriver;

architecture Behavioral of vhdlDriver is

signal clk25              : std_logic;
signal clk50             : std_logic;
signal horizontal_counter : std_logic_vector (9 downto 0);
signal vertical_counter   : std_logic_vector (9 downto 0);
begin

process (clk)
begin
  if clk'event and clk='1' then
    if (clk50 = '0') then
      clk50 <= '1';
    else
      clk50 <= '0';
    end if;
  end if;
end process;


process (clk50)
begin
  if clk50'event and clk50='1' then
    if (clk25 = '0') then
      clk25 <= '1';
    else
      clk25 <= '0';
    end if;
  end if;
end process;

process (clk25)
begin
  if clk25'event and clk25 = '1' then
    -- width 640 height 480
    if (horizontal_counter >= "0010010000" ) -- 144
    and (horizontal_counter < "0111010000" ) -- 464
    and (vertical_counter >= "0000100111" ) -- 39
    and (vertical_counter < "0100010111" ) -- 279
    then
      vgaRed <= "0000";
      vgaGreen <= "1111";
      vgaBlue <= "0000";
 
    elsif (horizontal_counter >= "0111010000" ) -- 464
    and (horizontal_counter < "1100010000" ) -- 784
    and (vertical_counter >= "0000100111" ) -- 279
    and (vertical_counter < "0100010111" ) -- 519
    then
    vgaRed <= "1111";
    vgaGreen <= "0000";
    vgaBlue <= "0000";
    elsif (horizontal_counter >= "0010010000" ) -- 144
    and (horizontal_counter < "0111010000" ) -- 464
    and (vertical_counter >= "0100010111" ) -- 39
    and (vertical_counter < "1000000111" ) -- 279
    then
        vgaRed <= "0000";
        vgaGreen <= "0000";
        vgaBlue <= "1111";
    elsif (horizontal_counter >= "0111010000" ) -- 144
    and (horizontal_counter < "1100010000" ) -- 464
    and (vertical_counter >= "0100010111" ) -- 39
    and (vertical_counter < "1000000111" ) -- 279
    then
        vgaRed <= "1111";
        vgaGreen <= "1111";
        vgaBlue <= "1111";
    else
    vgaRed <= "0000";
    vgaGreen <= "0000";
    vgaBlue <= "0000";
      
    end if;
    if (horizontal_counter > "0000000000" )
      and (horizontal_counter < "0001100001" ) -- 96+1
    then
      Hsync <= '0';
    else
      Hsync <= '1';
    end if;
    if (vertical_counter > "0000000000" )
      and (vertical_counter < "0000000011" ) -- 2+1
    then
      Vsync <= '0';
    else
      Vsync <= '1';
    end if;
    horizontal_counter <= horizontal_counter+"0000000001";
    if (horizontal_counter="1100100000") then
      vertical_counter <= vertical_counter+"0000000001";
      horizontal_counter <= "0000000000";
    end if;
    if (vertical_counter="1000001001") then
      vertical_counter <= "0000000000";
    end if;
  end if;
end process;

end Behavioral;
