----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.03.2016 17:38:30
-- Design Name: 
-- Module Name: core - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;



-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity core is
    Port ( led : out STD_LOGIC_VECTOR (11 downto 0);
            btnC: in STD_LOGIC;
            clk: in STD_LOGIC
          );
end core;

architecture Behavioral of core is

component blk_mem_gen_0 IS
  PORT (
    clka : IN STD_LOGIC;
    addra : IN STD_LOGIC_VECTOR(16 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(11 DOWNTO 0)
  );
end component;

signal addra: STD_LOGIC_VECTOR(16 downto 0):= "00000000000000000";
signal clk1 : STD_LOGIC := '0';
signal count : integer :=1;

begin

blkMem: blk_mem_gen_0
    port map(
    clka => clk,
    addra => addra,
    douta => led
    );

process(clk)
begin
    if(clk'event and clk='1') then
    count <=count+1;
        if(count = 50000000) then
        clk1 <= not clk1;
        count <=1;
        end if;
    end if;
end process;
process (clk1)
begin
    if(rising_edge(clk1)) then
        addra <= addra + 1;
    end if;
end process;
end Behavioral;
